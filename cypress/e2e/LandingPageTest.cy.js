/// <reference types="cypress"/>

describe('Test suite for Lambdatest landing page test.',()=>{
    beforeEach('',()=>{
        cy.visit('/')
    })
    it('Verify landing page title and end-point',()=>{
        cy.title('Your Store').should('eq','Your Store')
        cy.url().should('eq','https://ecommerce-playground.lambdatest.io/index.php/')
    })
    it('Verify 50% discount on message on page',()=>{
        cy.get('.mb-4 > h4').should('contain.text','Upto 50% Off on Fully Automatic Top Load Washing Machine')
    })
    it('Verify burger menu can be clicked and assert there is a button with  Mice and Trackballs text and click the button',()=>{
        cy.get('#entry_217833 > a').click()
        cy.contains('.nav-item > a > .info > span', 'Mice and Trackballs')
        cy.contains('.nav-item > a > .info > span', 'Mice and Trackballs').click()
        cy.get('#entry_212392 > h1').should('contain.text','Mice and Trackballs')
    })
    it('Verify there are 15 products listed when the Mice and Track page loads',()=>{
        cy.get('#entry_217833 > a').click()
        cy.contains('.nav-item > a > .info > span', 'Mice and Trackballs').click()
        cy.get('div.row > .product-layout').its('length').should('eql',15)
    })
    it('Adding items to cart from the Mice and Trackballs page',()=>{
        //Click on burger menu on landing page
        cy.get('#entry_217833 > a').click()
        //Click on the Mice and Trackballs button on the burger menu
        cy.contains('.nav-item > a > .info > span', 'Mice and Trackballs').click()
        //Add item with title HTC Touch HD to cart
        cy.contains('div.row > .product-layout','HTC Touch HD').within(()=>{
            cy.get('span:contains("Add to Cart")').should('have.length',1).click({force:true})
            })
        //Add item with title iPod Touch to cart
        cy.contains('div.row > .product-layout','iPod Touch').within(()=>{
            cy.get('span:contains("Add to Cart")').should('have.length',1).click({force:true})
            })
        // Verify the number of items added to cart
          cy.get('#entry_217825 > .cart > .cart-icon > .badge').should('contain.text','2')
        //Click on page 4 button of the product list
        cy.contains('#entry_212409 > .row > .col-sm-6 > .pagination > .page-item > a','4').click()
        cy.url().should('eq','https://ecommerce-playground.lambdatest.io/index.php?route=product/category&path=25_29&page=4')
    })
})