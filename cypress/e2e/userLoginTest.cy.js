/// <reference types ="cypress" />
describe('User login test',()=>{
    it('User Login test',()=>{
        cy.visit('/')
        cy.get('#widget-navbar-217834 > .navbar-nav > :nth-child(6) > .nav-link')
        cy.get('.mz-sub-menu-96').invoke('show')
        cy.contains('Login').click({force:true})
        cy.fixture('userdata').then(userdata =>{
            const email = userdata.loginUserEmail
            const password = userdata.password
        cy.get('#input-email').type(email)
        cy.get('#input-password').type(password)
        })
        cy.get('input[type=submit]').click()
        cy.get('#content > .card > h2').should('contain.text','My Account')
        cy.url().should('include','account')
    })
})