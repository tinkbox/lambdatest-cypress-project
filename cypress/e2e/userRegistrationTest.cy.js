/// <reference types = "cypress" />
describe('User registration test',()=>{

    var ranEmail = Math.random().toString(36).substring(2)
    it('User registration',()=>{
        cy.visit('/')
        cy.get('#widget-navbar-217834 > .navbar-nav > :nth-child(6) > .nav-link')
        cy.get('.mz-sub-menu-96').invoke('show')
        cy.contains('Register').click({force:true})
        cy.fixture('userdata').then(userdata =>{
            const firstName = userdata.firstName
            const lastName = userdata.lastName
            const email = ranEmail+'@gmail.com'
            const telephone = userdata.telephone
            const password = userdata.password
        cy.get('#input-firstname').type(firstName)
        cy.get('#input-lastname').type(lastName)
        cy.get('#input-email').type(email)
        cy.get('#input-telephone').type(telephone)
        cy.get('#input-password').type(password)
        cy.get('#input-confirm').type(password)
        })
        cy.get('#input-agree').check({force:true})
        cy.get('input[type=submit]').click()
        cy.get('#content > h1').should('contain.text',' Your Account Has Been Created!')
    })
})